FROM bitnami/nginx
USER root
ARG USER=jobtech
ARG PASSWD=jobtech1
ENV USER=$USER
ENV PASSWD=$PASSWD

COPY  ./dist /opt/bitnami/nginx/html/historicalskills
#COPY  ./src /opt/bitnami/nginx/html
COPY  ./vhosts /tmp/conf/vhosts

RUN apt-get update && export DEBIAN_FRONTEND=noninteractive && apt-get -yq install apache2-utils 
RUN rm -f /tmp/conf/vhosts/vhost.conf;\
   mkdir /opt/bitnami/nginx/conf/vhosts ;\
   cp /tmp/conf/vhosts/vhosts.conf /opt/bitnami/nginx/conf/server_blocks/ ;\
   mv /tmp/conf/vhosts/vhosts.conf /opt/bitnami/nginx/conf/vhosts/ ;

ENTRYPOINT [ "/opt/bitnami/scripts/nginx/entrypoint.sh" ]
CMD [ "/opt/bitnami/scripts/nginx/run.sh" ]

